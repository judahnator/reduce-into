<?php

namespace judahnator;

/**
 * Similar to array_reduce, but allows you to reduce an iterable item into any variable.
 * The $callback should accept a reference to the carry, the iterable input value, then key.
 *
 * @see array_reduce()
 *
 * @param iterable $input
 * @param callable $callback
 * @param mixed $carry
 * @return mixed
 */
function reduce_into(iterable $input, callable $callback, $carry = null)
{
    foreach ($input as $k => $v) {
        $callback($carry, $v, $k);
    }
    return $carry;
}
