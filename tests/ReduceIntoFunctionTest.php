<?php


namespace judahnator\ReduceInto\Tests;

use function judahnator\reduce_into;
use PHPUnit\Framework\TestCase;
use stdClass;

final class ReduceIntoFunctionTest extends TestCase
{
    public function testReducingToSingleValue(): void
    {
        // Basically identical to normal array_reduce.

        // Strings
        $this->assertEquals(
            'foobarbaz',
            reduce_into(
                [
                    'foo',
                    'bar',
                    'baz'
                ],
                function (?string &$carry, string $input): void {
                    $carry .= $input;
                }
            )
        );

        // Numbers
        $this->assertEquals(
            42,
            reduce_into(
                [
                    2,
                    3,
                    7
                ],
                function (?int &$carry, int $input): void {
                    $carry ? $carry *= $input : $carry = $input;
                }
            )
        );

        // With initial
        $this->assertEquals(
            'initial',
            reduce_into(
                ['initial'],
                function (string $initial, string $input): void {
                    $this->assertEquals($input, $initial);
                },
                'initial'
            )
        );
    }

    public function testReducingToArray(): void
    {
        $this->assertEquals(
            [
                3 => 2,
                4 => 1
            ],
            reduce_into(
                [
                    'foo',
                    'bar',
                    'bing'
                ],
                function (array &$carry, string $value): void {
                    $wordLength = strlen($value);
                    if (!array_key_exists($wordLength, $carry)) {
                        $carry[$wordLength] = 0;
                    }
                    $carry[$wordLength]++;
                },
                []
            )
        );
    }

    public function testReducingToObject(): void
    {
        $this->assertEquals(
            (object)[
                'foo' => 'bar'
            ],
            reduce_into(
                [
                    'foo' => 'bar'
                ],
                function (stdClass &$carry, string $item, string $key): void {
                    $carry->{$key} = $item;
                },
                new stdClass()
            )
        );
    }
}
